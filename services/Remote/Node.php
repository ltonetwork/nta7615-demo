<?php

declare(strict_types=1);

namespace App\Services\Remote;

use App\Services\Remote\Node\TransactionFailedException;
use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

class Node
{
    public const ASSOCIATION_TYPES = [
        'issuer' => 10,
        'provider' => 100,
        'client' => 200,
    ];

    protected HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function issueAssociation(string $type, string $sender, string $party): void
    {
        $this->post('associations/issue', $this->getPostData($type, $sender, $party));
    }

    public function revokeAssociation(string $type, string $sender, string $party): void
    {
        $this->post('associations/revoke', $this->getPostData($type, $sender, $party));
    }

    protected function getPostData(string $type, string $sender, string $party): array
    {
        if (!isset(self::ASSOCIATION_TYPES[$type])) {
            throw new \InvalidArgumentException("Unknown association type '$type'");
        }

        return [
            'version' => 1,
            'sender' => $sender,
            'party' => $party,
            'associationType' => self::ASSOCIATION_TYPES[$type],
            'fee' => 100000000,
            'hash' => '',
        ];
    }

    protected function post(string $url, array $data)
    {
        try {
            $response = $this->httpClient->request('POST', $url, ['json' => $data]);
        } catch (ClientException $exception) {
            $message = $this->parseJsonBody($exception->getResponse(), $exception);
            throw new TransactionFailedException($message["message"], $message['error'] ?? 0, $exception);
        }

        return $this->parseJsonBody($response);
    }

    protected function parseJsonBody(ResponseInterface $response, ?\RuntimeException $exception = null): array
    {
        $contentType = trim(explode(';', $response->getHeaderLine('Content-Type'), 2)[0]);

        if ($contentType !== 'application/json') {
            throw ($exception ?? new \UnexpectedValueException("Expected JSON response, got $contentType"));
        }

        $body = (string)$response->getBody();

        return json_decode($body, true, 16, JSON_THROW_ON_ERROR);
    }
}
