<?php

declare(strict_types=1);

namespace App\Services\Remote\Node;

class TransactionFailedException extends \RuntimeException
{
}
