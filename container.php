<?php

declare(strict_types=1);

use Improved\IteratorPipeline\Pipeline;
use Jasny\Container\AutowireContainerInterface;
use Jasny\Container\Container;
use Psr\Container\ContainerInterface;
use RemotelyLiving\PHPDNS\Resolvers as PHPDNS;

return new Container([
    'config' => static function () {
        return (new Jasny\Config())->load('config/settings.yml');
    },
    'addresses' => static function () {
        return yaml_parse_file('config/addresses.yml');
    },
    'routes' => static function () {
        return yaml_parse_file('config/routes.yml');
    },

    Jasny\Autowire\AutowireInterface::class => static function (ContainerInterface $container) {
        return new Jasny\Autowire\ReflectionAutowire($container);
    },
    Jasny\SwitchRoute\Generator::class => static function () {
        $stud = fn($str) => strtr(ucwords($str, '-'), ['-' => '']);
        $camel = fn($str) => strtr(lcfirst(ucwords($str, '-')), ['-' => '']);

        $invoker = new Jasny\SwitchRoute\Invoker(function (?string $controller, ?string $action) use ($stud, $camel) {
            return $controller !== null
                ? ['App\\Actions\\' . $stud($controller) . 'Controller', $camel($action ?? 'default')]
                : ['App\\Actions\\' . $stud($action) . 'Action', 'run'];
        });

        return new Jasny\SwitchRoute\Generator(
            new Jasny\SwitchRoute\Generator\GenerateFunction($invoker)
        );
    },

    Twig\Environment::class => static function(ContainerInterface $container) {
        $loader = new Twig\Loader\FilesystemLoader('views');
        $cache = APP_ENV === 'dev'
            ? new Twig\Cache\NullCache()
            : new Twig\Cache\FilesystemCache('generated/views');

        $twig = new Twig\Environment($loader, ['cache' => $cache]);

        $twig->addGlobal('addresses', $container->get('addresses'));
        $twig->addGlobal('links', $container->get('config.links'));
        $twig->addGlobal('debugbar', $container->get(DebugBar\JavascriptRenderer::class));

        return $twig;
    },

    PHPDNS\Interfaces\DNSQuery::class => static function() {
        return new PHPDNS\LocalSystem();
    },
    GuzzleHttp\HandlerStack::class => static function(ContainerInterface $container) {
        /** @var Psr\Log\LoggerInterface&DebugBar\DataCollector\DataCollectorInterface $logger */
        $logger = $container->get(DebugBar\DebugBar::class)->getCollector('API requests');
        $formatter = new GuzzleHttp\MessageFormatter("{request}\n\n{response}");

        $stack = GuzzleHttp\HandlerStack::create();
        $stack->push(GuzzleHttp\Middleware::log($logger, $formatter));

        return $stack;
    },
    App\Services\Remote\Node::class => static function (ContainerInterface $container) {
        $httpClient = new GuzzleHttp\Client([
            'base_uri' => $container->get('config.remote.node.url'),
            'headers' => [
                'X-API-Key' => $container->get('config.remote.node.api_key'),
            ],
            'handler' => $container->get(GuzzleHttp\HandlerStack::class),
        ]);

        return new App\Services\Remote\Node($httpClient);
    },
    App\Services\Remote\Anchoring::class => static function (ContainerInterface $container) {
        $httpClient = new GuzzleHttp\Client([
            'base_uri' => $container->get('config.remote.anchoring.url'),
            'handler' => $container->get(GuzzleHttp\HandlerStack::class),
        ]);

        return new App\Services\Remote\Anchoring($httpClient);
    },
    App\Services\RecipientVerifier::class => static function (AutowireContainerInterface $container) {
        return $container->autowire(App\Services\RecipientVerifier::class);
    },

    DebugBar\DebugBar::class => static function (ContainerInterface $container) {
        $addresses = Pipeline::with($container->get('addresses'))->flatten(true)->flip()->toArray();

        return (new DebugBar\DebugBar())
            ->addCollector(new DebugBar\DataCollector\PhpInfoCollector())
            ->addCollector(new class('API requests') extends DebugBar\DataCollector\MessagesCollector {
                public function log($level, $message, array $context = []) {
                    $this->addMessage($message, $level, false); // Don't collapse to single line
                }
            })
            ->addCollector(new DebugBar\DataCollector\ConfigCollector($addresses, 'LTO Addresses'));
    },
    DebugBar\JavascriptRenderer::class => static function (ContainerInterface $container) {
        $debugbarRenderer = $container->get(DebugBar\DebugBar::class)->getJavascriptRenderer();
        $debugbarRenderer->setBaseUrl("https://cdn.jsdelivr.net/gh/maximebf/php-debugbar/src/DebugBar/Resources/");
        $debugbarRenderer->disableVendor('highlightjs');

        return $debugbarRenderer;
    },
]);
