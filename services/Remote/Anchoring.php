<?php

declare(strict_types=1);

namespace App\Services\Remote;

use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class Anchoring
{
    protected HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Get associations for the given address on LTO Network.
     *
     * @param string $address
     * @return array{parents:array<string>,children:array<string>}
     * @throws GuzzleException
     * @throws \UnexpectedValueException
     */
    public function getAssociations(string $address): array
    {
        $response = $this->httpClient->request('GET', 'associations/' . $address);

        return $this->parseJsonBody($response);
    }

    protected function parseJsonBody(ResponseInterface $response): array
    {
        $contentType = trim(explode(';', $response->getHeaderLine('Content-Type'), 2)[0]);

        if ($contentType !== 'application/json') {
            throw new \UnexpectedValueException("Expected JSON response, got $contentType");
        }

        return json_decode((string)$response->getBody(), true, 16, JSON_THROW_ON_ERROR);
    }
}
