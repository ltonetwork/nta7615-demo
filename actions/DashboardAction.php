<?php

declare(strict_types=1);

namespace App\Actions;

use Twig\Environment as Twig;

class DashboardAction
{
    protected Twig $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function run(): void
    {
        echo $this->twig->render('dashboard.html.twig');
    }
}
