<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\X509;
use Improved\IteratorPipeline\Pipeline;
use App\Models\VerificationResult;
use App\Services\Remote\Anchoring;
use RemotelyLiving\PHPDNS\Entities\DNSRecord;
use RemotelyLiving\PHPDNS\Resolvers\Interfaces\DNSQuery;
use Spatie\Regex\Regex;
use function App\get_lto_addr;

/**
 * Verify an email recipient for NTA 7516 using DNS MX and LTO Network associations.
 */
class RecipientVerifier
{
    protected DNSQuery $dnsQuery;
    protected Anchoring $anchoring;

    public function __construct(DNSQuery $dnsQuery, Anchoring $anchoring)
    {
        $this->dnsQuery = $dnsQuery;
        $this->anchoring = $anchoring;
    }

    public function verify(string $email): VerificationResult
    {
        $result = new VerificationResult();
        $result->email = $email;
        $result->domain = explode('@', $email, 2)[1];

        $result->mx = $this->getMxRecords($result->domain);

        $result->domainAddress = get_lto_addr($result->domain);
        $result->providers = $this->anchoring->getAssociations($result->domainAddress)['parents'];

        if ($result->providers !== []) {
            $result->cert = $this->getCert($result->providers[0]);
        }

        return $result;
    }

    protected function getMxRecords(string $domain): array
    {
        try {
            $dnsRecords = $this->dnsQuery->getMXRecords($domain);
        } catch (\Exception $e) {
            trigger_error("Failed to get MX records from DNS for '$domain': " . $e->getMessage(), E_USER_WARNING);
            return [];
        }

        return Pipeline::with($dnsRecords)
            ->map(fn(DNSRecord $record) => (string)$record->getData())
            ->sort(SORT_NATURAL, false)
            ->toArray();
    }

    /**
     * Get an X501 certificate for a provider.
     * How this should happen is still undetermined. For now we'll just get it from the local filesystem.
     */
    protected function getCert(string $provider): ?X509
    {
        if (!Regex::match('/\w+/', $provider)->hasMatch()) {
            throw new \InvalidArgumentException("Invalid provider address");
        }

        if (!file_exists("config/certs/$provider.crt")) {
            return null;
        }

        $pem = file_get_contents("config/certs/$provider.crt");

        if ($pem === false) {
            throw new \RuntimeException("Failed to read 'config/certs/$provider.crt'");
        }

        return new X509($pem);
    }
}
