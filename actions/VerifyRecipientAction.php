<?php

declare(strict_types=1);

namespace App\Actions;

use App\Services\RecipientVerifier;
use Twig\Environment as Twig;

class VerifyRecipientAction
{
    protected Twig $twig;
    protected RecipientVerifier $verifier;

    public function __construct(Twig $twig, RecipientVerifier $verifier)
    {
        $this->twig = $twig;
        $this->verifier = $verifier;
    }

    public function run(): void
    {
        $context = $this->verifyRecipient() + ['email' => $_POST['email'] ?? ''];

        echo $this->twig->render('dashboard.html.twig', $context);
    }

    protected function verifyRecipient(): array
    {
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

        if (!is_string($email)) {
            return $email === null ? [] : ['email_error' => true];
        }

        return [
            'result' => $this->verifier->verify($email),
        ];
    }
}
