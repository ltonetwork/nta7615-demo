<?php

declare(strict_types=1);

namespace App\Models\X509;

class Identity
{
    public string $country;
    public string $state;
    public string $location;
    public string $organization;
    public string $commonName;

    /**
     * @param array<string,string> $info
     */
    public function __construct(array $info)
    {
        $this->country = $info['C'] ?? '';
        $this->state = $info['ST'] ?? '';
        $this->location = $info['L'] ?? '';
        $this->organization = $info['O'] ?? '';
        $this->commonName = $info['CN'] ?? '';
    }

    public function __toString(): string
    {
        return join(', ', [
            "C = {$this->country}",
            "ST = {$this->state}",
            "L = {$this->location}",
            "O = {$this->organization}",
            "CN = {$this->commonName}"
        ]);
    }
}
