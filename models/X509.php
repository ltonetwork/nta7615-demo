<?php

declare(strict_types=1);

namespace App\Models;

use Spatie\Regex\Regex;
use function Jasny\str_ends_with;

/**
 * X509 certificate.
 */
class X509
{
    public X509\Identity $subject;
    public X509\Identity $issuer;

    public \DateTimeInterface $validFrom;
    public \DateTimeInterface $validTo;

    public string $signatureType;
    public ?string $publickey;

    public function __construct(string $pem)
    {
        $cert = openssl_x509_read($pem);

        if ($cert === false) {
            throw new \InvalidArgumentException("Invalid PEM encoded certificate");
        }

        $info = openssl_x509_parse($cert);

        $this->subject = new X509\Identity($info['subject']);
        $this->issuer = new X509\Identity($info['issuer']);

        $this->validFrom = (new \DateTimeImmutable())->setTimestamp($info['validFrom_time_t']);
        $this->validTo = (new \DateTimeImmutable())->setTimestamp($info['validTo_time_t']);

        $this->signatureType = $info['signatureTypeSN'];
        $this->publickey = $this->extractPublicKey($cert);
    }

    /**
     * Extract public key from cert resource.
     *
     * @param resource $cert
     * @return string  Binary
     */
    protected function extractPublicKey($cert): string
    {
        $pkeyResource = openssl_get_publickey($cert);
        $pemKey = openssl_pkey_get_details($pkeyResource)['key'] ?? '';
        $matchResult = Regex::match('#-----BEGIN PUBLIC KEY-----\n([\w\+/=]+)\n-----END PUBLIC KEY-----#', $pemKey);

        if (!$matchResult->hasMatch()) {
            throw new \UnexpectedValueException("Failed to extract public key from certificate");
        }

        $base64 = $matchResult->group(1);

        return substr(base64_decode($base64), 12);
    }

    /**
     * Get base58 encode public key.
     */
    public function getBase58PublicKey(): string
    {
        return base58_encode($this->publickey);
    }

    /**
     * Doe the certificate apply to the mx record / domain?
     */
    public function appliesTo(string $mx): bool
    {
        if ($this->subject->commonName === '') {
            return false;
        }

        $mxDomain = Regex::replace('/^\d+\s+|\.$/', '', $mx)->result();

        return str_ends_with($mxDomain, $this->subject->commonName);
    }
}
