![NTA 7516 demo](https://gitlab.com/ltonetwork/nta7615-demo/uploads/540d9b9a7f511d2051d5d320a2e6e557/screenshot-localhost_8000-2020.01.31-23_01_06.png)

Requirements
---

* PHP 7.4+
* OpenSSL 1.1.1 (with Ed25519 support)

_Required PHP extensions are listed in composer.json_

Installation
---

```
git clone git@gitlab.com:ltonetwork/nta7615-demo.git
cd nta7615-demo
composer install
```
Run
---

### Development

    php -S localhost:8000 app.php

### Nginx

```
server {
  listen         80 default_server;
  listen         [::]:80 default_server;

  location / {
    fastcgi_pass    unix:/run/php/php7.4-fpm.sock;
    include         fastcgi_params;
    fastcgi_param   SCRIPT_FILENAME  /var/www/nta7615-demo/app.php;
  }
}
```

### Apache

```
<VirtualHost *:80>
  DocumentRoot /var/www/html
  ProxyPassMatch ^.*$ unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost/var/www/nta7615-demo/app.php
</VirtualHost>
```
