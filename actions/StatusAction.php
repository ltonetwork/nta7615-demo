<?php

declare(strict_types=1);

namespace App\Actions;

use App\Services\Remote\Anchoring;
use Twig\Environment as Twig;

class StatusAction
{
    protected Twig $twig;
    protected Anchoring $anchoring;
    protected string $rootAddress;

    /**
     * @param Twig      $twig
     * @param Anchoring $anchoring
     * @param array     $addresses  "addresses"
     */
    public function __construct(Twig $twig, Anchoring $anchoring, array $addresses)
    {
        $this->twig = $twig;
        $this->anchoring = $anchoring;
        $this->rootAddress = key($addresses['root']);
    }

    public function run(string $type)
    {
        $address = filter_input(INPUT_POST, 'as', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\w{35}$/']]);
        if (!is_string($address)) {
            http_response_code(400);
            echo "Invalid input";
            return;
        }

        $associations = $this->anchoring->getAssociations($address);

        $parents = array_filter(
            $associations['parents'],
            fn ($addr) => $addr === $this->rootAddress xor $type !== 'issuer'
        );

        $status = $parents !== [] ? 'ok' : 'fail';

        echo $this->twig->render('dashboard.html.twig', ['as' => $address, 'status' => $status]);
    }
}
