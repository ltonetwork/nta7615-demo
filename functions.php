<?php

declare(strict_types=1);

namespace App;

use function sodium_crypto_generichash as blake2b;

/**
 * SHA256 hashing with raw output.
 */
function sha256(string $seed): string
{
    return hash('sha256', $seed, true);
}

/**
 * Create an LTO network address from a seed (eg the domain).
 */
function get_lto_addr(string $seed): string
{
    $hash = substr(sha256(blake2b($seed)), 0, 20);
    $prefix = pack('Ca', 0x1, 'T');

    $base = $prefix . $hash;
    $checksum = substr(sha256(blake2b($base)), 0, 4);

    return base58_encode($base . $checksum);
}
