<?php

declare(strict_types=1);

use Jasny\Container\Container;
use Jasny\SwitchRoute\Generator as RouteGenerator;

ini_set('display_errors', 'off');

require_once 'vendor/autoload.php';

define('APP_ENV', (getenv('APPLICATION_ENV') ?: 'dev'));

$path = rawurldecode(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH));

/** @var Container $container */
$container = require 'container.php';

$container->get(RouteGenerator::class)
    ->generate('route', 'generated/route.php', fn () => $container->get('routes'), APP_ENV === 'dev');

require_once 'generated/route.php';

route($_SERVER["REQUEST_METHOD"], $path, fn($class) => $container->autowire($class));
