<?php

declare(strict_types=1);

namespace App\Models;

use function Jasny\str_before;

class VerificationResult
{
    public string $email;
    public string $domain;
    public string $domainAddress;

    /** @var string[] */
    public array $mx = [];

    /** @var string[] */
    public array $providers = [];
    public ?X509 $cert = null;

    /**
     * Does MX record have highest priority?
     */
    public function isPrimaryMx(string $mx): bool
    {
        return str_before($mx, ' ') === str_before($this->mx[0] ?? '', ' ');
    }
}
