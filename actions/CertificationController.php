<?php

declare(strict_types=1);

namespace App\Actions;

use App\Services\Remote\Node;
use Twig\Environment as Twig;

class CertificationController
{
    protected Twig $twig;
    protected Node $node;

    public function __construct(Twig $twig, Node $node)
    {
        $this->twig = $twig;
        $this->node = $node;
    }

    public function issue(string $type): void
    {
        $this->run($type, function (string $sender, string $party) use ($type) {
            $this->node->issueAssociation($type, $sender, $party);
            return ['type' => "success", 'message' => "Association issued successfully"];
        });
    }

    public function revoke(string $type): void
    {
        $this->run($type, function (string $sender, string $party) use ($type) {
            $this->node->revokeAssociation($type, $sender, $party);
            return ['type' => "success", 'message' => "Association revoked successfully"];
        });
    }

    protected function run(string $type, callable $invoke): void
    {
        if ($type !== 'issuer' && $type !== 'provider') {
            http_response_code(404);
            echo "Invalid association type";
            return;
        }

        $sender = filter_input(INPUT_POST, 'as', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\w{35}$/']]);
        $party = filter_input(INPUT_POST, 'party', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\w{35}$/']]);

        if (!is_string($sender) || !is_string($party)) {
            http_response_code(400);
            echo "Invalid input";
            return;
        }

        try {
            $notification = $invoke($sender, $party);
        } catch (Node\TransactionFailedException $exception) {
            $notification = ['type' => "danger", 'message' => $exception->getMessage()];
        }

        $context = [
            'as' => $sender,
            'party' => $party,
            'notification' => $notification,
        ];

        echo $this->twig->render('dashboard.html.twig', $context);
    }
}
