<?php

declare(strict_types=1);

namespace App\Actions;

use App\Services\Remote\Node;
use Twig\Environment as Twig;
use function App\get_lto_addr;

class ClientController
{
    protected Twig $twig;
    protected Node $node;

    public function __construct(Twig $twig, Node $node)
    {
        $this->twig = $twig;
        $this->node = $node;
    }

    public function issue()
    {
        $this->run('issue');
    }

    public function revoke()
    {
        $this->run('revoke');
    }

    protected function run(string $method)
    {
        $sender = filter_input(INPUT_POST, 'as', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\w{35}$/']]);
        if (!is_string($sender)) {
            http_response_code(400);
            echo "Invalid input";
            return;
        }

        $domain = filter_input(INPUT_POST, 'domain', FILTER_VALIDATE_DOMAIN);
        if (!is_string($domain)) {
            echo $this->twig->render('dashboard.html.twig', ['error_domain' => true]);
            return;
        }
        $party = get_lto_addr($domain);

        try {
            $this->node->{$method . 'Association'}('client', $sender, $party);
            $notification = ['type' => "success", 'message' => "Association for '{$domain}' {$method}d successfully"];
        } catch (Node\TransactionFailedException $exception) {
            $notification = ['type' => "danger", 'message' => $exception->getMessage()];
        }

        $context = [
            'as' => $sender,
            'notification' => $notification,
        ];

        echo $this->twig->render('dashboard.html.twig', $context);
    }
}
